#!/bin/bash

docker run -it \
    --gpus "\"device=0,1,2,3\"" \
    -v models:/models \
    --name xdc_engine \
    docker.maum.ai:443/brain/brain_xdc:1.1.1-packed
