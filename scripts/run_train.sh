#!/bin/bash
# export CUDA_VISIBLE_DEVICES=0

CFG="cfg/config.yml"
MODEL_CFG="cfg/model.yml"

python src/runner.py --run train \
    --cfg ${CFG} \
    --model_cfg ${MODEL_CFG} \
