#!/bin/bash

REGISTRY=docker.maum.ai:443
IMAGE_NAME=brain/brain_xdc
VERSION=1.1.1
FULL_IMAGE=${REGISTRY}/${IMAGE_NAME}:${VERSION}

# check below path
PRETRAINED_CKPT=/raid/pretrained/bert_kor_mecab 

docker build -f Dockerfile -t ${FULL_IMAGE} . 
docker build -f Dockerfile-server -t ${FULL_IMAGE}-server .

WRAP_CONTAINER=wrap_xdc-${VERSION}
docker run -it -d --name ${WRAP_CONTAINER} ${FULL_IMAGE} 
docker exec ${WRAP_CONTAINER} mkdir /pretrained
docker cp ${PRETRAINED_CKPT} ${WRAP_CONTAINER}:/pretrained/bert_kor_mecab
docker commit ${WRAP_CONTAINER} ${FULL_IMAGE}-packed

docker stop ${WRAP_CONTAINER} 
docker rm ${WRAP_CONTAINER}
