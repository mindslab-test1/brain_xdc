#!/bin/bash

PROTO_PARENT='src'
python -m grpc.tools.protoc --proto_path ${PROTO_PARENT} \
    ${PROTO_PARENT}/proto/xdc.proto \
    --python_out ${PROTO_PARENT} \
    --grpc_python_out ${PROTO_PARENT} \
