# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: proto/xdc.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='proto/xdc.proto',
  package='maum.brain.bert.xdc',
  syntax='proto3',
  serialized_options=None,
  serialized_pb=_b('\n\x0fproto/xdc.proto\x12\x13maum.brain.bert.xdc\"\x1c\n\tInputText\x12\x0f\n\x07\x63ontext\x18\x01 \x01(\t\"\xa0\x01\n\x07Outputs\x12-\n\x06labels\x18\x01 \x03(\x0b\x32\x1d.maum.brain.bert.xdc.Classify\x12\x32\n\nword_idxes\x18\x02 \x03(\x0b\x32\x1e.maum.brain.bert.xdc.Attention\x12\x32\n\nsent_idxes\x18\x03 \x03(\x0b\x32\x1e.maum.brain.bert.xdc.Attention\"\'\n\x08\x43lassify\x12\r\n\x05label\x18\x01 \x01(\t\x12\x0c\n\x04prob\x18\x02 \x01(\x02\"7\n\tAttention\x12\r\n\x05start\x18\x01 \x01(\x05\x12\x0b\n\x03\x65nd\x18\x02 \x01(\x05\x12\x0e\n\x06weight\x18\x03 \x01(\x02\x32Q\n\x07\x42\x65rtXDC\x12\x46\n\x06\x41nswer\x12\x1e.maum.brain.bert.xdc.InputText\x1a\x1c.maum.brain.bert.xdc.Outputsb\x06proto3')
)




_INPUTTEXT = _descriptor.Descriptor(
  name='InputText',
  full_name='maum.brain.bert.xdc.InputText',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='context', full_name='maum.brain.bert.xdc.InputText.context', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=40,
  serialized_end=68,
)


_OUTPUTS = _descriptor.Descriptor(
  name='Outputs',
  full_name='maum.brain.bert.xdc.Outputs',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='labels', full_name='maum.brain.bert.xdc.Outputs.labels', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='word_idxes', full_name='maum.brain.bert.xdc.Outputs.word_idxes', index=1,
      number=2, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='sent_idxes', full_name='maum.brain.bert.xdc.Outputs.sent_idxes', index=2,
      number=3, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=71,
  serialized_end=231,
)


_CLASSIFY = _descriptor.Descriptor(
  name='Classify',
  full_name='maum.brain.bert.xdc.Classify',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='label', full_name='maum.brain.bert.xdc.Classify.label', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='prob', full_name='maum.brain.bert.xdc.Classify.prob', index=1,
      number=2, type=2, cpp_type=6, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=233,
  serialized_end=272,
)


_ATTENTION = _descriptor.Descriptor(
  name='Attention',
  full_name='maum.brain.bert.xdc.Attention',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='start', full_name='maum.brain.bert.xdc.Attention.start', index=0,
      number=1, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='end', full_name='maum.brain.bert.xdc.Attention.end', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='weight', full_name='maum.brain.bert.xdc.Attention.weight', index=2,
      number=3, type=2, cpp_type=6, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=274,
  serialized_end=329,
)

_OUTPUTS.fields_by_name['labels'].message_type = _CLASSIFY
_OUTPUTS.fields_by_name['word_idxes'].message_type = _ATTENTION
_OUTPUTS.fields_by_name['sent_idxes'].message_type = _ATTENTION
DESCRIPTOR.message_types_by_name['InputText'] = _INPUTTEXT
DESCRIPTOR.message_types_by_name['Outputs'] = _OUTPUTS
DESCRIPTOR.message_types_by_name['Classify'] = _CLASSIFY
DESCRIPTOR.message_types_by_name['Attention'] = _ATTENTION
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

InputText = _reflection.GeneratedProtocolMessageType('InputText', (_message.Message,), dict(
  DESCRIPTOR = _INPUTTEXT,
  __module__ = 'proto.xdc_pb2'
  # @@protoc_insertion_point(class_scope:maum.brain.bert.xdc.InputText)
  ))
_sym_db.RegisterMessage(InputText)

Outputs = _reflection.GeneratedProtocolMessageType('Outputs', (_message.Message,), dict(
  DESCRIPTOR = _OUTPUTS,
  __module__ = 'proto.xdc_pb2'
  # @@protoc_insertion_point(class_scope:maum.brain.bert.xdc.Outputs)
  ))
_sym_db.RegisterMessage(Outputs)

Classify = _reflection.GeneratedProtocolMessageType('Classify', (_message.Message,), dict(
  DESCRIPTOR = _CLASSIFY,
  __module__ = 'proto.xdc_pb2'
  # @@protoc_insertion_point(class_scope:maum.brain.bert.xdc.Classify)
  ))
_sym_db.RegisterMessage(Classify)

Attention = _reflection.GeneratedProtocolMessageType('Attention', (_message.Message,), dict(
  DESCRIPTOR = _ATTENTION,
  __module__ = 'proto.xdc_pb2'
  # @@protoc_insertion_point(class_scope:maum.brain.bert.xdc.Attention)
  ))
_sym_db.RegisterMessage(Attention)



_BERTXDC = _descriptor.ServiceDescriptor(
  name='BertXDC',
  full_name='maum.brain.bert.xdc.BertXDC',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  serialized_start=331,
  serialized_end=412,
  methods=[
  _descriptor.MethodDescriptor(
    name='Answer',
    full_name='maum.brain.bert.xdc.BertXDC.Answer',
    index=0,
    containing_service=None,
    input_type=_INPUTTEXT,
    output_type=_OUTPUTS,
    serialized_options=None,
  ),
])
_sym_db.RegisterServiceDescriptor(_BERTXDC)

DESCRIPTOR.services_by_name['BertXDC'] = _BERTXDC

# @@protoc_insertion_point(module_scope)
