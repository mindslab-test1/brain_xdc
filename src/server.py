# MindsLab Inc. 
# Explainable Document Classifier.
#
# ver 1.1.0

import argparse, logging, sys, time, grpc
from concurrent import futures
from datetime import datetime
from omegaconf import OmegaConf
from pathlib import Path

from runner import XdcRunner, setup_config, setup_logger, logger

from proto.xdc_pb2 import Outputs, Classify, Attention
from proto.xdc_pb2_grpc import BertXDCServicer, add_BertXDCServicer_to_server

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

class BertXDC(BertXDCServicer):
    def __init__(self, cfg):
        self.runner = XdcRunner(cfg)

        logger.info('Warming up the GPU with a dummy data')
        _ = self.runner.infer(['Dummy text for warmup'])
        logger.info('Done warming up')

    def Answer(self, msg, context):
        results = self.runner.infer([msg.context])
        logger.debug(results)

        outputs = self._postprocess(results)

        return outputs

    def _postprocess(self, results):
        predictions = []
        word_idxes = []
        sent_idxes = []
        for top_preds, words_info, sents_info in results:
            predictions = [Classify(label=pred, prob=prob) for pred, prob in top_preds]
            word_attns = [Attention(start=start, end=end, weight=weight) for (start, end), word, weight in words_info]
            sent_attns = [Attention(start=start, end=end, weight=weight) for (start, end), sent, weight in sents_info]

        # TODO multiple outputs
        outputs = Outputs(
            labels = predictions,
            word_idxes = word_attns,
            sent_idxes = sent_attns,
        )

        return outputs


# define and read arguments especially for running, not modeling
def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_path', type=str, required=True)
    parser.add_argument('--log_level', type=str, default='INFO', choices=['INFO', 'DEBUG'])

    parser.add_argument('--port', type=int, required=True)

    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = parse_args()
    args.run = 'serve'

    cfg = setup_config(args)
    setup_logger(cfg)

    logger.info('Initializing xdc engine')
    bertxdc = BertXDC(cfg)

    logger.info('Building grpc server')
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1), )
    add_BertXDCServicer_to_server(bertxdc, server)

    server.add_insecure_port('[::]:{}'.format(args.port))
    server.start()

    logger.info('Xdc Server starting at 0.0.0.0:{}'.format(args.port))

    try:
        while True:
            # Sleep forever, since `start` doesn't block
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        logger.info('Shutting down the server')
        server.stop(0)
