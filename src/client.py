import grpc
import argparse
from proto import xdc_pb2
from proto import xdc_pb2_grpc

def request(context, host, port):
    with grpc.insecure_channel('{}:{}'.format(host, port)) as channel:
        stub = xdc_pb2_grpc.BertXDCStub(channel)
        input_text = xdc_pb2.InputText(context=context)

        return stub.Answer(input_text)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', type=str, default="localhost")
    parser.add_argument('--port', type=int, required=True)
    parser.add_argument('--passage', type=str, default="XDC 테스트용 텍스트입니다. 원하는 텍스트를 입력하세요.")

    args = parser.parse_args()
    
    outputs = request(args.passage, args.host, args.port)

    print('[Passage]:\n {}'.format(args.passage))
    print('[Label prediction]')
    for prediction in outputs.labels:
        print(' label: %s - prob: %.6f' % (prediction.label, prediction.prob))
    print('[top_k sentences]')
    for idx, sent_info in enumerate(outputs.sent_idxes):
        print(' start:', sent_info.start)
        print(' end:', sent_info.end)
        print(' weight: %.6f' % sent_info.weight)
    print('[top_k words]')
    for idx, word_info in enumerate(outputs.word_idxes):
        print(' start:', word_info.start)
        print(' end:', word_info.end)
        print(' weight: %.6f' % word_info.weight)
